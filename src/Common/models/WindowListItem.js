export default class WindowListItem {
  constructor(windowId) {
    this.id = windowId;
    this.tabs = [];
  }

  addTab(tab) {
    this.tabs.push(tab);
  }

  addTabs(tabs) {
    this.tabs = this.tabs.concat(tabs);
  }
}
