import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

import App from './Options.vue';
import router from './router';

Vue.use(BootstrapVue);

/* eslint-disable no-new */
new Vue({
  el: '#options',
  router,
  render: h => h(App),
});
