import Vue from 'vue';
import VueRouter from 'vue-router';
import routes from './routes';
import _ from 'lodash';

// Setup Lodash
Object.defineProperty(Vue.prototype, '$_', { value: _ });

Vue.use(VueRouter);

export default new VueRouter({
  routes,
});
