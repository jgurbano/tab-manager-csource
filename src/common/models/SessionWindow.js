export default class SessionWindow {
  constructor(incognito, tabs) {
    this.incognito = incognito;
    this.tabs = tabs;
  }

  setTabs(tabs) {
    this.tabs = tabs;
  }
}
