module.exports = {
  preset: '@vue/cli-plugin-unit-jest/presets/no-babel',
  collectCoverageFrom: ['./src/**/*.{js,vue}', '!**/node_modules/**'],
  coverageDirectory: './coverage',
  moduleDirectories: ['node_modules', 'src'],
  modulePaths: ['src'],
  resolver: null,
  transform: {
    '^.+\\.js$': '<rootDir>/node_modules/babel-jest',
    '.*\\.(vue)$': '<rootDir>/node_modules/jest-vue-preprocessor',
  },
};
