import * as OmniBox from 'OmniBox.js';
import { COMMANDS_TEXT, CLOSE_ALL_TABS, CLOSE_OTHER_WINDOWS, CLOSE_THIS_WINDOW, CLOSE_OTHER_TABS } from 'Common/Constants';
import * as imports from 'imports';
const chrome = require('sinon-chrome');

/**
 * TEST
 */
describe('OmniBox.js', () => {
  beforeAll(() => {
    global.chrome = chrome;
  });

  afterAll(() => {
    delete global.chrome;
  });

  beforeEach(() => {
    jest.resetModules();

    chrome.flush();
    OmniBox.resetSuggestions();
    OmniBox.registerEvents();
  });

  it('Testing constructor', () => {
    expect(chrome.omnibox.setDefaultSuggestion.calledOnceWith({ description: COMMANDS_TEXT })).toBeTruthy();
  });

  it('Test registerEvent()', () => {
    expect(chrome.omnibox.onInputChanged.addListener.calledOnce).toBeTruthy();
    expect(chrome.omnibox.onInputCancelled.addListener.calledOnce).toBeTruthy();
    expect(chrome.omnibox.onInputEntered.addListener.calledOnce).toBeTruthy();
  });

  test.each`
    input                  | funcToBeCalled
    ${CLOSE_ALL_TABS}      | ${'closeAllTabs'}
    ${CLOSE_OTHER_WINDOWS} | ${'closeOtherWindows'}
    ${CLOSE_THIS_WINDOW}   | ${'closeThisWindow'}
    ${CLOSE_OTHER_TABS}    | ${'closeOtherTabs'}
  `('Test onInputEntered event', ({ input, funcToBeCalled }) => {
    // jest.mock('@/imports', () => {
    //   return {
    //     ChromeTabService: {
    //       funcToBeCalled: jest.fn(),
    //     },
    //   };
    // });
    let spy = jest.spyOn(imports.ChromeTabService, funcToBeCalled);
    chrome.omnibox.onInputEntered.dispatch(input, '');
    expect(spy).toHaveBeenCalled();
  });

  test.each`
    input | expected     | expectedDefault
    ${''} | ${undefined} | ${COMMANDS_TEXT}
  `('Test onInputChanged event', ({ input, expected, expectedDefault }) => {
    let suggestions;
    let suggest = s => {
      suggestions = s;
    };
    chrome.omnibox.setDefaultSuggestion.flush();

    chrome.omnibox.onInputChanged.dispatch(input, suggest);
    expect(suggestions).toBe(expected);
    expect(chrome.omnibox.setDefaultSuggestion.calledOnceWith({ description: expectedDefault })).toBeTruthy();
  });
});
