export default class SessionTab {
  constructor(icon, title, url, highlighted) {
    this.icon = icon;
    this.title = title;
    this.url = url;
    this.highlighted = highlighted;
  }
}
