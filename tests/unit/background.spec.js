const chrome = require('sinon-chrome');

/**
 * Mock data
 */

let windows = [
  {
    tabs: [1, 2, 3],
  },
  {
    tabs: [1, 2],
  },
];

/**
 * TEST
 */
describe('background.js', () => {
  beforeAll(() => {
    global.chrome = chrome;
  });

  beforeEach(() => {
    jest.resetModules();
    chrome.flush();
    chrome.windows.getAll.yields(windows);

    jest.mock('@/OmniBox.js', () => {
      return {
        registerEvents: jest.fn(),
        resetSuggestions: jest.fn(),
      };
    });
    require('@/background.js');
  });

  afterAll(() => {
    delete global.chrome;
  });

  it('Testing omnibox registration', () => {
    let OmniBox = require('@/OmniBox.js');
    expect(OmniBox.registerEvents).toHaveBeenCalled();
    expect(OmniBox.resetSuggestions).toHaveBeenCalled();
  });

  it('Test updateExtensionBadge', () => {
    expect(chrome.browserAction.setBadgeText.calledOnceWith({ text: '5' })).toBeTruthy();
  });

  it('Test chrome.tabs.onRemoved listener', () => {
    chrome.browserAction.setBadgeText.flush();
    chrome.runtime.sendMessage.flush();

    chrome.windows.getAll.yields([{ tabs: [1] }]);
    chrome.tabs.onRemoved.dispatch();
    expect(chrome.runtime.sendMessage.calledOnceWith({ message: 'updateList' })).toBeTruthy();
    expect(chrome.browserAction.setBadgeText.calledOnceWith({ text: '1' })).toBeTruthy();
  });

  it('Test chrome.tabs.onCreated listener', () => {
    chrome.browserAction.setBadgeText.flush();
    chrome.runtime.sendMessage.flush();

    chrome.windows.getAll.yields([{ tabs: [1] }]);
    chrome.tabs.onCreated.dispatch();
    expect(chrome.runtime.sendMessage.calledOnceWith({ message: 'updateList' })).toBeTruthy();
    expect(chrome.browserAction.setBadgeText.calledOnceWith({ text: '1' })).toBeTruthy();
  });

  it('Test chrome.tabs.onUpdated listener', () => {
    chrome.browserAction.setBadgeText.flush();
    chrome.runtime.sendMessage.flush();

    chrome.windows.getAll.yields([{ tabs: [1] }]);
    chrome.tabs.onUpdated.dispatch();
    expect(chrome.runtime.sendMessage.calledOnceWith({ message: 'updateList' })).toBeTruthy();
    expect(chrome.browserAction.setBadgeText.calledOnceWith({ text: '1' })).toBeTruthy();
  });
});
