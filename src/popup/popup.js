import Vue from 'vue';
import Vuex from 'vuex';
import PopupApp from './PopupApp';
import router from './router';
import store from './router/store';

import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue';

Vue.use(BootstrapVue);
Vue.use(BootstrapVueIcons);
Vue.use(Vuex);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(PopupApp),
});
