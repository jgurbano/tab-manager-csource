import * as OmniBox from './OmniBox.js';

OmniBox.resetSuggestions();
OmniBox.registerEvents();

/**
 * Main section
 */

updateExtensionBadge();

/**
 * Listeners
 */

chrome.tabs.onRemoved.addListener(() => updateList());
chrome.tabs.onCreated.addListener(() => updateList());
chrome.tabs.onUpdated.addListener(() => updateList());

chrome.windows.onRemoved.addListener(() => updateList());
chrome.windows.onCreated.addListener(() => updateList());

/**
 * Private functions
 */

function updateExtensionBadge() {
  let total = 0;
  chrome.windows.getAll({ populate: true }, windows => {
    windows.forEach(window => {
      total += window.tabs.length;
    });
    chrome.browserAction.setBadgeText({ text: total.toString() });
  });
}

function updateList() {
  chrome.runtime.sendMessage({ message: 'updateList' });
  updateExtensionBadge();
}
