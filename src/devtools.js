var LOGGER_ID = 'logger';
chrome.devtools.panels.create('Omage TabM', null, './devtools.html');
let id = 1;

chrome.runtime.onMessage.addListener(function(message, sender, sendResponse) {
  if (message && message.id === LOGGER_ID) {
    appendToPage(createEntry(message.message));
  }

  window.scrollTo(0, document.body.scrollHeight);
});

function createEntry(text) {
  let index = text.indexOf('->') + 2;
  let logInfo = text.substr(0, index);
  let logText = text.replace(logInfo, '').trim();

  let div = document.createElement('div');
  div.id = `div${id}`;

  let shouldAddExpandButton = addExpandButton(logText);
  if (shouldAddExpandButton) {
    let toggleChecker = document.createElement('p');
    toggleChecker.id = `toggle${id}`;
    toggleChecker.className = `hidden`;
    toggleChecker.innerText = '0';

    div.appendChild(toggleChecker);
  }

  div.appendChild(createExpandButton(id, shouldAddExpandButton));

  div.appendChild(createSpan(logInfo, `logInfo${id}`));
  div.appendChild(createSpan(logText, `logText${id}`));
  div.appendChild(document.createElement('hr'));
  id++;
  return div;
}

function createSpan(text, id) {
  var s = document.createElement('span');
  s.id = id;
  s.innerText = text;
  return s;
}

function appendToPage(element) {
  var mainDiv = document.getElementById('main');
  mainDiv.appendChild(element);
}

function addExpandButton(text) {
  try {
    JSON.parse(text);
    return true;
  } catch (_) {
    return false;
  }
}

function createExpandButton(idVal, shouldAddExpandButton) {
  let button = document.createElement('span');
  button.className = shouldAddExpandButton ? 'expandButton' : 'placeholderButton';
  button.innerText = shouldAddExpandButton ? '+' : '';
  if (shouldAddExpandButton) {
    button.onclick = function() {
      var p = document.getElementById(`logText${idVal}`);
      var toggle = document.getElementById(`toggle${idVal}`);
      try {
        if (toggle.innerText === '0') {
          p.innerText = JSON.stringify(JSON.parse(p.innerText), null, 2);
          toggle.innerText = '1';
        } else {
          p.innerText = JSON.stringify(JSON.parse(p.innerText));
          toggle.innerText = '0';
        }
      } catch (_) {}
    };
  }

  return button;
}
