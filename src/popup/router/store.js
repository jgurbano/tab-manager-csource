import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    openDomains: {},
  },
  getters: {
    openDomains(state) {
      return state.openDomains;
    },
  },
  mutations: {
    updateOpenDomains(state, value) {
      state.openDomains[value.name] = value.isOpen;
    },
  },
  actions: {
    updateOpenDomains(context, value) {
      context.commit('updateOpenDomains', value);
    },
  },
});
