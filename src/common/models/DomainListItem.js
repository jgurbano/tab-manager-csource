export default class DomainListItem {
  constructor(icon, url) {
    this.id = Math.floor(Math.random() * 1000000) + 1 + '';
    this.icon = icon || './../images/empty.svg';
    this.name = url;
    this.tabs = [];
  }

  addTab(tab) {
    this.tabs.push(tab);
  }

  addTabs(tabs) {
    this.tabs = this.tabs.concat(tabs);
  }
}
