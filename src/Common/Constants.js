export let CLOSE_ALL_TABS = 'Close all tabs';
export let CLOSE_OTHER_TABS = 'Close other tabs';
export let CLOSE_OTHER_WINDOWS = 'Close other windows';
export let CLOSE_THIS_WINDOW = 'Close this window';

export let COMMANDS_TEXT = 'Start typing to see all available commands...';
export let CHOOSE_COMMAND_TEXT = 'Choose a command';

export let LOGGER_ID = 'logger';

export let DARK_THEME = 'dark-theme';
export let LIGHT_THEME = 'light-theme';
export let THEME_CHANGE_EVENT = 'themeChanged';
