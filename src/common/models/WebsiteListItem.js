export default class WebsiteListItem {
  constructor(id, windowId, index, icon, title, isHighlighted, isIncognito, soundOn, isLoading) {
    this.id = id;
    this.windowId = windowId;
    this.index = index;
    this.icon = icon || './../images/empty.svg';
    this.name = title;
    this.isHighlighted = isHighlighted;
    this.isIncognito = isIncognito;
    this.soundOn = soundOn;
    this.isLoading = isLoading;
  }
}
