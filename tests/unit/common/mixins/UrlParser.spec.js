import UrlParser from '@/common/mixins/UrlParser.vue';

describe('UrlParser.vue', () => {
  test.each`
    input                             | expected
    ${'https://www.google.com/'}      | ${'www.google.com'}
    ${'https://www.google.com/hello'} | ${'www.google.com'}
    ${'https://'}                     | ${''}
    ${'https:/www.google.com'}        | ${''}
    ${'file:///C:\\SomeLocation'}     | ${'Local and Shared Files'}
    ${''}                             | ${''}
    ${'notUrl'}                       | ${''}
    ${{ url: 'hello' }}               | ${''}
  `('Test domainFromUrl()', ({ input, expected }) => {
    expect(UrlParser.domainFromUrl(input)).toMatch(expected);
  });
});
