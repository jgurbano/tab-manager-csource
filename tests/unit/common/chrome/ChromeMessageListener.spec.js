import ChromeMessageListener from '@/common/chrome/ChromeMessageListener.vue';
const chrome = require('sinon-chrome');

/**
 * TEST
 */
describe('ChromeMessageListener.vue', () => {
  beforeAll(() => {
    global.chrome = chrome;
  });

  beforeEach(() => {
    chrome.flush();
  });

  afterAll(() => {
    delete global.chrome;
  });

  it('Testing emit function', () => {
    let msg = 'Message';
    ChromeMessageListener.emit(msg);
    expect(chrome.runtime.sendMessage.calledOnceWith({ message: msg })).toBeTruthy();
  });

  it('Testing on function', () => {
    let msg = 'Message';
    let called = false;
    let cb = function() {
      called = true;
    };
    ChromeMessageListener.on(msg, cb);
    chrome.runtime.onMessage.dispatch({ message: 'msg' });
    expect(called).toBeFalsy();

    chrome.runtime.onMessage.dispatch({ message: msg });
    expect(called).toBeTruthy();
  });
});
