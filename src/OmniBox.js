import * as Constants from './common/Constants';
import { ChromeTabService } from './imports';

const _commands = [Constants.CLOSE_ALL_TABS, Constants.CLOSE_OTHER_TABS, Constants.CLOSE_OTHER_WINDOWS, Constants.CLOSE_THIS_WINDOW];

function _createSuggestions(text, suggest) {
  if (!text || text.length === 0) {
    _resetSuggestions();
    return;
  }

  // load of suggestions
  let suggestions = [];

  for (let value of _commands) {
    let d = _createDesctiontion(text, value);
    if (value.toLowerCase().search(text.toLowerCase()) !== -1) {
      suggestions.splice(0, 0, d);
    } else {
      suggestions.push(d);
    }
  }

  chrome.omnibox.setDefaultSuggestion({
    description: Constants.CHOOSE_COMMAND_TEXT,
  });

  if (suggest) suggest(suggestions);
}

function _createDesctiontion(text, description) {
  let originalText = description;

  if (typeof text !== 'string' || typeof description !== 'string') {
    return {};
  }

  if (!text || text.length < 3) {
    return { content: originalText, description: description };
  }

  text = text.toLowerCase();
  description = description.toLowerCase();

  let index = description.search(text);
  if (index === -1) {
    return { content: originalText, description: description };
  }

  var str;
  // at the start
  if (index === 0) {
    let match = description.substr(index, text.length);
    let dim = description.replace(text, '');
    str = `<match>${match}</match><dim>${dim}</dim>`;
  }
  // at the end
  else if (index + text.length === description.length) {
    let match = description.substr(index, text.length);
    let dim = description.replace(text, '');
    str = `<dim>${dim}</dim><match>${match}</match>`;
  }
  // middle
  else {
    let leftSide = description.substr(0, index);
    let match = description.substr(index, text.length);
    let rightSide = description.replace(leftSide + match, '');
    str = `<dim>${leftSide}</dim><match>${match}</match><dim>${rightSide}</dim>`;
  }

  return { content: originalText, description: str };
}

function _resetSuggestions() {
  chrome.omnibox.setDefaultSuggestion({
    description: Constants.COMMANDS_TEXT,
  });
}

function _selectionMade(text, disposition) {
  switch (text) {
    case Constants.CLOSE_ALL_TABS:
      ChromeTabService.closeAllTabs();
      break;
    case Constants.CLOSE_OTHER_WINDOWS:
      ChromeTabService.closeOtherWindows();
      break;
    case Constants.CLOSE_THIS_WINDOW:
      ChromeTabService.closeThisWindow();
      break;
    case Constants.CLOSE_OTHER_TABS:
      ChromeTabService.closeOtherTabs();
      break;
    default:
    // Figure out how to notify user
  }
}

export let resetSuggestions = _resetSuggestions;

export let registerEvents = function() {
  chrome.omnibox.onInputChanged.addListener(_createSuggestions);
  chrome.omnibox.onInputCancelled.addListener(_resetSuggestions);
  chrome.omnibox.onInputEntered.addListener(_selectionMade);
};
